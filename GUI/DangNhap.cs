﻿using BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class DangNhap : Form
    {
        public DangNhap()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text))
            {
                if (BUSDN.cPrimaryKey(textBox1.Text.Trim(),textBox2.Text))
                {
                    FormMain fM = new FormMain();
                    fM.ShowDialog();
                    textBox1.Clear();
                    textBox2.Clear();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào sai tên tài khoản hoặc mật khẩu.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void DangNhap_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DoimatKhau dmk = new DoimatKhau();
            dmk.Show();
        }
    }
}
