﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Luongcoban : Form
    {
        public Luongcoban()
        {
            InitializeComponent();
        }

        private void HopDongNV_Load(object sender, EventArgs e)
        {
            Load1();
        }
        public Luongcoban(string tex,string text): this()
        {
            textBox1.Text = tex;
            label9.Text = text;
        }
        private void Load1()
        {
            string t = textBox1.Text;
            dataGridView1.DataSource = BUSLuongcoban.loadLuong(t);
        }
        private string phucap1()
        {
            string s;
            if(textBox5.Text==" ")
            {
                s = " ";
            }
            else
            {
                s = textBox5.Text;
            }
            return s;
        }
        private string phucap2()
        {
            string s;
            if (textBox6.Text == " ")
            {
                s = " ";
            }
            else
            {
                s = textBox6.Text;
            }
            return s;
        }
        private string phucap3()
        {
            string s;
            if (textBox7.Text == " ")
            {
                s = " ";
            }
            else
            {
                s = textBox7.Text;
            }
            return s;
        }
        private void xoa()
        {
            textBox2.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            dateTimePicker1.ResetText();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string ma = textBox1.Text;
            string ten = label9.Text;
            string Ngaybd = dateTimePicker1.Text;
            string Luongcb = textBox2.Text;
            string Luongbhxh = textBox4.Text;
            string pc1 = phucap1();
            string pc2 = phucap2();
            string pc3 = phucap3();
            LuongCoBan1 lcb = new LuongCoBan1(ma, ten, Ngaybd, Luongcb, Luongbhxh, pc1, pc2, pc3);
            BUSLuongcoban.addLuong(lcb);
            Load1();
            xoa();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ma = textBox1.Text;
            string ngay = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            BUSLuongcoban.deleteNV(ma,ngay);
            Load1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            dateTimePicker1.Text= dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox4.Text= dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox5.Text= dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox6.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox7.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
        }
    }
}
