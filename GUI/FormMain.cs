﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NienKhoa1 nk = new NienKhoa1();
            nk.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            HocSinh hs = new HocSinh();
            hs.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NhanVien1 nv = new NhanVien1();
            nv.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ChiaPhong cp = new ChiaPhong();
            cp.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DiemDanh dd = new DiemDanh();
            dd.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            KhauPhanAn dd = new KhauPhanAn();
            dd.ShowDialog();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
        }
    }
}
