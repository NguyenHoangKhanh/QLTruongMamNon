﻿using BUS;
using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class KhauPhanAn : Form
    {
        public KhauPhanAn()
        {
            InitializeComponent();
        }

        private void KhauPhanAn_Load(object sender, EventArgs e)
        {
            SelectNamHoc();
            dataGridView1.DataSource = BUSCheDoDinhDuong.loadCDDD();
        }
        private void SelectNamHoc()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("SelectDaTa", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);
            comboBox1.DisplayMember = "Namhoc";
            comboBox1.DataSource = ds.Tables[0].AsDataView();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox5.Text) )
            {
                if (!BUSCheDoDinhDuong.cPrimaryKey(textBox6.Text))
                {
                    string NamHoc = comboBox1.GetItemText(comboBox1.SelectedItem);
                    string Tuan = textBox1.Text.Trim();
                    string Ngay = dateTimePicker1.Text;
                    string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
                    string MaLop = textBox6.Text;
                    string Buoisang = textBox2.Text.Trim();
                    string BuoiTrua = textBox3.Text.Trim();
                    string BuoiChieu = textBox4.Text.Trim();
                    string TongTien = textBox5.Text.Trim();
                    CheDoDinhDuong cddd = new CheDoDinhDuong(NamHoc,Tuan,Ngay,Lop,MaLop,Buoisang,BuoiTrua,BuoiChieu,TongTien);
                    BUSCheDoDinhDuong.addCDDD(cddd);
                    dataGridView1.DataSource = BUSCheDoDinhDuong.loadCDDD();
                    XoaText();
                }
                else
                {
                    MessageBox.Show("Dữ liệu đã có.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }
        private void XoaText()
        {
            textBox1.Clear();
            textBox2.Clear();
            dateTimePicker1.ResetText();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox1.GetItemText(comboBox1.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox2.Items.Clear();
            while (dr.Read())
            {
                comboBox2.Items.Add((string)dr["TenLop"]);
                comboBox2.SelectedIndex = 0;
            }
            Conn1.Close();
          
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            string Nam = comboBox1.GetItemText(comboBox1.SelectedItem);
            string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = Nam;
            cmd.Parameters["@TenLop"].Value = Lop;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox6.Text = (string)dr["MaLop"];
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string MaLop = textBox6.Text;
            BUSCheDoDinhDuong.deleteCDDD(MaLop);
            dataGridView1.DataSource = BUSCheDoDinhDuong.loadCDDD();
            XoaText();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            comboBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox1.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            dateTimePicker1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            comboBox2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox6.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string NamHoc = comboBox1.GetItemText(comboBox1.SelectedItem);
            string Tuan = textBox1.Text.Trim();
            string Ngay = dateTimePicker1.Text;
            string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
            string MaLop = textBox6.Text;
            string Buoisang = textBox2.Text.Trim();
            string BuoiTrua = textBox3.Text.Trim();
            string BuoiChieu = textBox4.Text.Trim();
            string TongTien = textBox5.Text.Trim();
            CheDoDinhDuong cddd = new CheDoDinhDuong(NamHoc, Tuan, Ngay, Lop, MaLop, Buoisang, BuoiTrua, BuoiChieu, TongTien);
            BUSCheDoDinhDuong.deleteCDDD(MaLop);
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox5.Text))
            {
                if (!BUSCheDoDinhDuong.cPrimaryKey(textBox6.Text))
                {
                    BUSCheDoDinhDuong.addCDDD(cddd);
                    dataGridView1.DataSource = BUSCheDoDinhDuong.loadCDDD();
                    XoaText();
                }
                else
                {
                    MessageBox.Show("Dữ liệu đã có.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
