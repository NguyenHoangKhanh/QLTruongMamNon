﻿using BUS;
using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class DiemDanh : Form
    {
        public DiemDanh()
        {
            InitializeComponent();
        }

        private void DiemDanh_Load(object sender, EventArgs e)
        {
            select();
        }
        private void select()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("SelectDaTa", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);
            comboBox2.DisplayMember = "Namhoc";
            comboBox2.DataSource = ds.Tables[0].AsDataView();
            comboBox5.DisplayMember = "Namhoc";
            comboBox5.DataSource = ds.Tables[0].AsDataView();
            Conn.Close();
        }
        private void Load1()
        {
            string Nam = comboBox2.GetItemText(comboBox2.SelectedItem);
            string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
            dataGridView1.DataSource = BUSDiemDanh.loadNV2(Nam, Lop);
            string Nam1 = comboBox5.GetItemText(comboBox5.SelectedItem);
            string Lop2 = comboBox4.GetItemText(comboBox4.SelectedItem);
            dataGridView4.DataSource = BUSDiemDanh.loadNV3(Nam1, Lop2);
            Load2();
        }
        private void Load2()
        {
            string Nam = comboBox2.GetItemText(comboBox2.SelectedItem);
            string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
            string tuan = textBox1.Text;
            string thu = dateTimePicker1.Text;
            dataGridView2.DataSource = BUSDiemDanh.loadNV1(tuan, thu, Nam, Lop);
            string Nam1 = comboBox5.GetItemText(comboBox5.SelectedItem);
            string Lop1 = comboBox4.GetItemText(comboBox4.SelectedItem);
            string tuan1 = textBox2.Text;
            string thu1 = dateTimePicker2.Text;
            dataGridView3.DataSource = BUSDiemDanh.loadNV4(tuan1, thu1, Nam1, Lop1);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox2.GetItemText(comboBox2.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox3.Items.Clear();
            while (dr.Read())
            {
                comboBox3.Items.Add((string)dr["TenLop"]);
                comboBox3.SelectedIndex = 0;
            }
            Conn1.Close();
            Load1();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load1();
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = comboBox2.GetItemText(comboBox2.SelectedItem);
            cmd.Parameters["@TenLop"].Value = comboBox3.GetItemText(comboBox3.SelectedItem);
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox3.Text = (string)dr["MaLop"];
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text))
            {
                if (!BUSDiemDanh.cPrimaryKey(dataGridView1.CurrentRow.Cells[0].Value.ToString(),dateTimePicker1.Text))
                {
                    string Manv = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    string Tennv = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    string Nam = comboBox2.GetItemText(comboBox2.SelectedItem);
                    string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
                    string tuan = textBox1.Text;
                    string thu = dateTimePicker1.Text;
                    DiemDanhGV nk = new DiemDanhGV(tuan,thu,Manv,Tennv,Nam,Lop,textBox3.Text);
                    BUSDiemDanh.Vang(nk);
                    dataGridView2.DataSource = BUSDiemDanh.loadNV1(tuan,thu,Nam,Lop);
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Load2();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string Manv = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            string thu = dateTimePicker1.Text;
            BUSDiemDanh.deleteNV(Manv,thu);
            Load1();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox5.GetItemText(comboBox5.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox4.Items.Clear();
            while (dr.Read())
            {
                comboBox4.Items.Add((string)dr["TenLop"]);
                comboBox4.SelectedIndex = 0;
            }
            Conn1.Close();
            Load1();
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load1();
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = comboBox5.GetItemText(comboBox5.SelectedItem);
            cmd.Parameters["@TenLop"].Value = comboBox4.GetItemText(comboBox4.SelectedItem);
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox4.Text = (string)dr["MaLop"];
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Load2();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            Load2();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox2.Text))
            {
                if (!BUSDiemDanh.cPrimaryKey1(dataGridView4.CurrentRow.Cells[0].Value.ToString(),dateTimePicker2.Text))
                {
                    string Manv = dataGridView4.CurrentRow.Cells[0].Value.ToString();
                    string Tennv = dataGridView4.CurrentRow.Cells[1].Value.ToString();
                    string Nam = comboBox5.GetItemText(comboBox5.SelectedItem);
                    string Lop = comboBox4.GetItemText(comboBox4.SelectedItem);
                    string tuan = textBox2.Text;
                    string thu = dateTimePicker2.Text;
                    string malop = textBox4.Text;
                    DiemDanhHS nk = new DiemDanhHS(tuan, thu, Manv, Tennv, Nam, Lop,malop);
                    BUSDiemDanh.Vang1(nk);
                    dataGridView3.DataSource = BUSDiemDanh.loadNV4(tuan, thu, Nam, Lop);
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string Manv = dataGridView3.CurrentRow.Cells[0].Value.ToString();
            string thu = dateTimePicker2.Text;
            BUSDiemDanh.deleteNV1(Manv,thu);
            Load1();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
