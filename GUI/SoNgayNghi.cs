﻿using BUS;
using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class SoNgayNghi : Form
    {
        public SoNgayNghi()
        {
            InitializeComponent();
        }
        private void Selectee()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("SelectDaTa", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);
            comboBox1.DisplayMember = "Namhoc";
            comboBox1.DataSource = ds.Tables[0].AsDataView();
        }
        private void SoNgayNghi_Load(object sender, EventArgs e)
        {
            Selectee();
            dataGridView1.DataSource = BUSSoNgayNghi.loadSNN(textBox1.Text);
            label7.Text = BUSSoNgayNghi.LoadLopp(textBox1.Text);
            int t = dataGridView1.RowCount - 1;
            textBox2.Text = t.ToString();
        }
        public SoNgayNghi(string s,string t): this()
        {
            textBox1.Text = s;
            label9.Text = t;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox1.GetItemText(comboBox1.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox2.Items.Clear();
            while (dr.Read())
            {
                comboBox2.Items.Add((string)dr["TenLop"]);
                comboBox2.SelectedIndex = 0;
            }
            Conn1.Close();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Nam = comboBox1.GetItemText(comboBox1.SelectedItem);
            string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = Nam;
            cmd.Parameters["@TenLop"].Value = Lop;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox3.Text = (string)dr["MaLop"];
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string manv = textBox1.Text;
            string nam = comboBox1.GetItemText(comboBox1.SelectedItem);
            string lop = comboBox2.GetItemText(comboBox2.SelectedItem);
            string malop = textBox3.Text;
            BUSSoNgayNghi.updatee(manv, nam, lop, malop);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
