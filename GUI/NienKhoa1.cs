﻿using BUS;
using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class NienKhoa1 : Form
    {
        public NienKhoa1()
        {
            InitializeComponent();
        }

        private void Them_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) )
            {
                if (!BUSNienKhoa.cPrimaryKey(textBox1.Text.Trim()))
                {
                    string NamH = textBox1.Text.Trim();
                    string Khoa = textBox2.Text.Trim();
                    string NgayBatDau = dateTimePicker1.Text;              
                    string NgayKetThuc = dateTimePicker2.Text;                   
                    NienKhoa nk = new NienKhoa(NamH,Khoa,NgayBatDau,NgayKetThuc);
                    BUSNienKhoa.addNam(nk);
                    dataGridView1.DataSource = BUSNienKhoa.loadData();
                    XoaText();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void NienKhoa1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BUSNienKhoa.loadData();
        }

        private void Xoa_Click(object sender, EventArgs e)
        {
            string namhoc = textBox1.Text;
            BUSNienKhoa.deleteNam(namhoc);
            dataGridView1.DataSource = BUSNienKhoa.loadData();
            XoaText();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            dateTimePicker1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            dateTimePicker2.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
        }
        private void XoaText()
        {
            textBox1.Clear();
            textBox2.Clear();
            dateTimePicker1.ResetText();
            dateTimePicker2.ResetText();
        }

        private void Capnhat_Click(object sender, EventArgs e)
        {
            string namH = textBox1.Text;
            string namH1 = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string khoa = textBox2.Text;
            string NgayBT = dateTimePicker1.Text;
            string NgayKT = dateTimePicker2.Text;
            NienKhoa nk = new NienKhoa( namH, khoa, NgayBT, NgayKT);
            BUSNienKhoa.deleteNam(namH1);
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text))
            {
                if (!BUSNienKhoa.cPrimaryKey(textBox1.Text.Trim()))
                {
                    BUSNienKhoa.addNam(nk);
                    dataGridView1.DataSource = BUSNienKhoa.loadData();
                    XoaText();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData1();
        }
        private void LoadData1()
        {
            string Select = comboBox1.GetItemText(comboBox1.SelectedItem);
            dataGridView2.DataSource = BUSNienKhoa.loadData1(Select);
        }

        private void tabPage2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("SelectDaTa", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);
            comboBox1.DisplayMember = "Namhoc";
            comboBox1.ValueMember = "TenLop";
            comboBox1.DataSource = ds.Tables[0].AsDataView();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox6.Text))
            {
                if (!BUSNienKhoa.cPrimaryKey1(textBox6.Text.Trim()))
                {
                    string NamH = comboBox1.GetItemText(comboBox1.SelectedItem);
                    string MaLop = textBox6.Text.Trim();
                    string TenLop = textBox3.Text.Trim();
                    Khoi k = new Khoi(MaLop,TenLop, NamH);
                    BUSNienKhoa.addKhoi(k);
                    LoadData1();
                    textBox3.Clear();
                    textBox6.Clear();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }
            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string MaLop = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            string Nam = comboBox1.GetItemText(comboBox1.SelectedItem);
            BUSNienKhoa.deleteKhoi(MaLop,Nam);
            LoadData1();
            textBox3.Clear();
            textBox6.Clear();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox3.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
            textBox6.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox6.Text) && !Catch.cNullTB(textBox3.Text))
            {
                if (textBox6.Text == dataGridView2.CurrentRow.Cells[0].Value.ToString())
            {
                string TenLop = textBox3.Text;
                string Malop = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                BUSNienKhoa.updateeKhoi(Malop, TenLop);
                    BUSNienKhoa.updateeCDDD(Malop, TenLop);
                    BUSNienKhoa.updateeChiaNV(Malop, TenLop);
                    BUSNienKhoa.updateeDDNV(Malop, TenLop);
                    BUSNienKhoa.updateeChiaHS(Malop, TenLop);
                    BUSNienKhoa.updateeDDHS(Malop, TenLop);
                    LoadData1();
                textBox3.Clear();
                textBox6.Clear();
            }
            else
            {
                MessageBox.Show("Không được thay đổi khóa chính.");
            }
            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    
}
