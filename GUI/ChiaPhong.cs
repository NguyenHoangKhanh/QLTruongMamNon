﻿using BUS;
using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class ChiaPhong : Form
    {
        public ChiaPhong()
        {
            InitializeComponent();
        }
        private void ChiaPhong_Load(object sender, EventArgs e)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("SelectDaTa", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataSet ds = new DataSet();
            da.Fill(ds);
            comboBox1.DisplayMember = "Namhoc";
            comboBox1.DataSource = ds.Tables[0].AsDataView();
            comboBox4.DisplayMember = "Namhoc";
            comboBox4.DataSource = ds.Tables[0].AsDataView();
            Conn.Close();
            dataGridView1.DataSource = BUSChiaPhong.loadNV1();
            dataGridView3.DataSource = BUSChiaPhong.loadNV4();
            Load1();
            Load2();
        }
        private void Load1()
        {
            string Nam = comboBox1.GetItemText(comboBox1.SelectedItem);
            string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
            dataGridView2.DataSource = BUSChiaPhong.loadNV2(Nam, Lop);         
            Count();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox1.GetItemText(comboBox1.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox2.Items.Clear();
            while (dr.Read())
            {
                comboBox2.Items.Add((string)dr["TenLop"]);
                comboBox2.SelectedIndex = 0;
            }
            Conn1.Close();
            Load1();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!BUSChiaPhong.cPrimaryKey(dataGridView1.CurrentRow.Cells[0].Value.ToString(),comboBox1.GetItemText(comboBox1.SelectedItem)))
            {
                string Manv = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                string Tennv = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                string Nam = comboBox1.GetItemText(comboBox1.SelectedItem);
                string Lop = comboBox2.GetItemText(comboBox2.SelectedItem);
                ChiaPhong1 nk = new ChiaPhong1(Nam,Lop,Manv,Tennv,textBox5.Text);
                BUSChiaPhong.addNV(nk);
                dataGridView2.DataSource = BUSChiaPhong.loadNV2(Nam,Lop);
                Load1();
            }
            else
            {
                MessageBox.Show("Trùng mã GV.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string Manv = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            BUSChiaPhong.deleteNV(Manv);
            Load1();
        }
        private void Count()
        {
            int t = dataGridView2.RowCount - 1;
            textBox2.Text = t.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BUSChiaPhong.loadNV3(textBox1.Text, textBox1.Text);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load1();
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = comboBox1.GetItemText(comboBox1.SelectedItem);
            cmd.Parameters["@TenLop"].Value = comboBox2.GetItemText(comboBox2.SelectedItem);
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox5.Text = (string)dr["MaLop"];
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            string namhoc = comboBox4.GetItemText(comboBox4.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectData1", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            comboBox3.Items.Clear();
            while (dr.Read())
            {
                comboBox3.Items.Add((string)dr["TenLop"]);
                comboBox3.SelectedIndex = 0;
            }
            Conn1.Close();
            Load2();
        }
        private void Count1()
        {
            int t = dataGridView4.RowCount - 1;
            textBox4.Text = t.ToString();
        }
        private void Load2()
        {
            string Nam = comboBox4.GetItemText(comboBox4.SelectedItem);
            string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
            dataGridView4.DataSource =BUSChiaPhong.loadNV5(Nam, Lop);
            Count1();
            Load3();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            dataGridView3.DataSource = BUSChiaPhong.loadNV6(textBox3.Text, textBox3.Text);
        }
        private void Load3()
        {
            string namhoc = comboBox4.GetItemText(comboBox4.SelectedItem);
            string lop= comboBox3.GetItemText(comboBox3.SelectedItem);
            SqlConnection Conn1 = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("TenNV", Conn1);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@Namhoc"].Value = namhoc;
            cmd.Parameters["@Lop"].Value = lop;
            Conn1.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            listView1.Clear();
            listView1.View = View.Tile;
            while (dr.Read())
            {
                listView1.Items.Add((string)dr["TenNV"]);
            }
            Conn1.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!BUSChiaPhong.cPrimaryKey1(dataGridView3.CurrentRow.Cells[0].Value.ToString(), comboBox4.GetItemText(comboBox4.SelectedItem)))
            {
                string Mahs = dataGridView3.CurrentRow.Cells[0].Value.ToString();
                string Tenhs = dataGridView3.CurrentRow.Cells[1].Value.ToString();
                string Nam = comboBox4.GetItemText(comboBox4.SelectedItem);
                string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
                ChiaPhong2 nk = new ChiaPhong2(Nam, Lop, Mahs, Tenhs,textBox6.Text);
                BUSChiaPhong.addHS(nk);
                dataGridView4.DataSource = BUSChiaPhong.loadNV5(Nam, Lop);
                Load2();
            }
            else
            {
                MessageBox.Show("Trùng mã HS.");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string Mahs = dataGridView4.CurrentRow.Cells[0].Value.ToString();
            BUSChiaPhong.deleteHS(Mahs);
            Load2();
        }

        private void tabPage2_MouseLeave(object sender, EventArgs e)
        {
            Load3();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Nam = comboBox4.GetItemText(comboBox4.SelectedItem);
            string Lop = comboBox3.GetItemText(comboBox3.SelectedItem);
            dataGridView4.DataSource = BUSChiaPhong.loadNV5(Nam, Lop);
            Count1();

            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("SelectKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = Nam;
            cmd.Parameters["@TenLop"].Value = Lop;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                textBox6.Text = (string)dr["MaLop"];
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
