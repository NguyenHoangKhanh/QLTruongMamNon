﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class NhanVien1 : Form
    {
        public NhanVien1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox5.Text) && !Catch.cNullTB(textBox6.Text))
            {
                if (!BUSNhanVien.cPrimaryKey(textBox1.Text.Trim()))
                {
                    string MaNV = textBox1.Text.Trim();
                    string TenNV = textBox2.Text.Trim();
                    string GioiTinh = gioitinh();
                    string NgaySinh = dateTimePicker1.Text;
                    string BangCap = textBox3.Text.Trim();
                    string Diachi = textBox4.Text.Trim();
                    string DT = textBox6.Text.Trim();
                    string Tinhtrang = textBox5.Text.Trim();
                    NhanVien nv = new NhanVien(MaNV,TenNV,NgaySinh,GioiTinh,BangCap,Diachi,DT,Tinhtrang);
                    BUSNhanVien.addNV(nv);
                    dataGridView1.DataSource = BUSNhanVien.loadNV();
                    XoaText();
                    Count();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }
        private string gioitinh()
        {
            string s;
            if (radioButton1.Checked == true)
            {
                s = radioButton1.Text;
            }
            else
            {
                s = radioButton2.Text;
            }
            return s;
        }
        private void XoaText()
        {
            textBox1.Clear();
            textBox2.Clear();
            dateTimePicker1.ResetText();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
        }
        private void Count()
        {
            int t = dataGridView1.RowCount - 1;
            textBox7.Text = t.ToString();
        }

        private void NhanVien1_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            dataGridView1.DataSource = BUSNhanVien.loadNV();
            Count();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string MaNV = textBox1.Text;
            BUSNhanVien.deleteNV(MaNV);
            dataGridView1.DataSource = BUSNhanVien.loadNV();
            XoaText();
            Count();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string MaNV = textBox1.Text.Trim();
            string manv= dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string TenNV = textBox2.Text.Trim();
            string GioiTinh = gioitinh();
            string NgaySinh = dateTimePicker1.Text;
            string BangCap = textBox3.Text.Trim();
            string Diachi = textBox4.Text.Trim();
            string DT = textBox6.Text.Trim();
            string Tinhtrang = textBox5.Text.Trim();
            NhanVien nv = new NhanVien(MaNV, TenNV, NgaySinh, GioiTinh, BangCap, Diachi, DT, Tinhtrang);
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox5.Text) && !Catch.cNullTB(textBox6.Text))
            {

                BUSNhanVien.updateNV(nv);
                BUSNhanVien.updateHD(MaNV,TenNV);
                BUSNhanVien.updateDDGV1(MaNV, TenNV);
                BUSNhanVien.updateChiaNV1(MaNV, TenNV);
                BUSNhanVien.updateLCB(MaNV, TenNV);
                dataGridView1.DataSource = BUSNhanVien.loadNV();
                XoaText();
                Count();
            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            textBox6.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            if (dataGridView1.CurrentRow.Cells[3].Value.ToString() == "Nam")
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            dateTimePicker1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            Luongcoban nv = new Luongcoban(s,t);
            nv.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            HopDongNV nv = new HopDongNV(s, t);
            nv.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            SoNgayNghi nv = new SoNgayNghi(s, t);
            nv.ShowDialog();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BUSNhanVien.loadNV1(textBox8.Text, textBox8.Text);
        }
    }
}
