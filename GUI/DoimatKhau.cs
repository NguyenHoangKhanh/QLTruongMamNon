﻿using BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class DoimatKhau : Form
    {
        public DoimatKhau()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text))
            {
                if (BUSDN.cPrimaryKey(textBox1.Text.Trim(),textBox2.Text))
                { 
                    if (textBox3.Text == textBox4.Text)
                    {
                        BUSDN.updatee(textBox1.Text, textBox4.Text);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Mật khẩu nhập lại không chính xác.");
                    }
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
