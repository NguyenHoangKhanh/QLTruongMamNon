﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class HopDongNV : Form
    {
        public HopDongNV()
        {
            InitializeComponent();
        }

        private void HopDongNV_Load(object sender, EventArgs e)
        {
            Load1();
        }
        public HopDongNV(string s,string t):this()
        {
            textBox1.Text = s;
            label5.Text = t;
        }
        private void Load1()
        {
            string t = textBox1.Text;
            dataGridView1.DataSource = BUSHopDongNV.loadHD(t);
        }
        private void xoa()
        {
            dateTimePicker1.ResetText();
            dateTimePicker2.ResetText();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string ma = textBox1.Text;
            string ten = label5.Text;
            string Ngaybd = dateTimePicker1.Text;
            string Ngaykt = dateTimePicker2.Text;
            HopDongNV1 hd = new HopDongNV1(ma, ten, Ngaybd, Ngaykt);
            BUSHopDongNV.addLuong(hd);
            Load1();
            xoa();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ma = textBox1.Text;
            BUSHopDongNV.deleteNV(ma);
            Load1();
            xoa();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
