﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DAL;
using DTO;

namespace GUI
{
    public partial class HocSinh : Form
    {
        public HocSinh()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox6.Text) && !Catch.cNullTB(textBox7.Text) && !Catch.cNullTB(textBox9.Text) && !Catch.cNullTB(textBox10.Text))
            {
                if (!BUSHocSinh.cPrimaryKey(textBox1.Text.Trim()))
                {
                    string MaHS = textBox1.Text.Trim();
                    string HoTen = textBox2.Text.Trim();
                    string GioiTinh = gioitinh();
                    string NgaySinh = dateTimePicker1.Text;
                    string HotenBo = textBox3.Text.Trim();
                    string nghenghiepbo = textBox4.Text.Trim();
                    string EmailBo = textBox5.Text.Trim();
                    string HoTenMe = textBox6.Text.Trim();
                    string NgheNghiepMe = textBox7.Text.Trim();
                    string EmailMe = textBox8.Text.Trim();
                    string SDT = textBox9.Text.Trim();
                    string DiaChi = textBox10.Text.Trim();
                    HocSinh1 hs = new HocSinh1(MaHS,HoTen,GioiTinh,NgaySinh,SDT,HotenBo,nghenghiepbo,EmailBo,HoTenMe,NgheNghiepMe,EmailMe,DiaChi);
                    BUSHocSinh.addHS(hs);
                    dataGridView1.DataSource = BUSHocSinh.loadHS();
                    XoaText();
                    Count();
                }
                else
                {
                    MessageBox.Show("Dữ liệu vừa nhập vào không hợp lệ, do bị trùng khóa chính.");
                }

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }
        private string gioitinh()
        {
            string s;
            if(radioButton1.Checked==true)
            {
                s = radioButton1.Text;
            }
            else
            {
                s = radioButton2.Text;
            }
            return s;
        }
        private void XoaText()
        {
            textBox1.Clear();
            textBox2.Clear();
            dateTimePicker1.ResetText();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
        }

        private void HocSinh_Load(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            dataGridView1.DataSource = BUSHocSinh.loadHS();
            Count();
        }
        private void Count()
        {
            int t = dataGridView1.RowCount - 1;
            textBox11.Text = t.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string MaHS = textBox1.Text;
            BUSHocSinh.deleteHS(MaHS);
            dataGridView1.DataSource = BUSHocSinh.loadHS();
            XoaText();
            Count();
        }
        private string text2()
        {
            string t;
            if (dataGridView1.CurrentRow.Cells[7].Value.ToString() == "Không có Email")
            {
                t = "";
            }
            else
            {
                t = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            }
            return t;
        }
        private string text3()
        {
            string t;
            if (dataGridView1.CurrentRow.Cells[10].Value.ToString() == "Không có Email")
            {
                t = "";
            }
            else
            {
                t = dataGridView1.CurrentRow.Cells[10].Value.ToString();
            }
            return t;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            textBox5.Text = text2();
            textBox6.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            textBox7.Text = dataGridView1.CurrentRow.Cells[9].Value.ToString();
            textBox8.Text = text3();
            textBox9.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox10.Text = dataGridView1.CurrentRow.Cells[11].Value.ToString();
            if(dataGridView1.CurrentRow.Cells[2].Value.ToString()=="Nam")
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            dateTimePicker1.Text= dataGridView1.CurrentRow.Cells[3].Value.ToString();

        }
        private string text()
        {
            string t;
            if(textBox5.Text=="")
            {
                t = "Không có Email";
            }
            else
            {
                t = textBox5.Text;
            }
            return t;
        }
        private string text1()
        {
            string t;
            if (textBox8.Text == "")
            {
                t = "Không có Email";
            }
            else
            {
                t = textBox8.Text;
            }
            return t;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string MaHS = textBox1.Text.Trim();
            string HoTen = textBox2.Text.Trim();
            string GioiTinh = gioitinh();
            string NgaySinh = dateTimePicker1.Text;
            string HotenBo = textBox3.Text.Trim();
            string nghenghiepbo = textBox4.Text.Trim();
            string EmailBo = text();
            string HoTenMe = textBox6.Text.Trim();
            string NgheNghiepMe = textBox7.Text.Trim();
            string EmailMe = text1();
            string SDT = textBox9.Text.Trim();
            string DiaChi = textBox10.Text.Trim();
            HocSinh1 hs = new HocSinh1(MaHS, HoTen, GioiTinh, NgaySinh, SDT, HotenBo, nghenghiepbo, EmailBo, HoTenMe, NgheNghiepMe, EmailMe, DiaChi);
            if (!Catch.cNullTB(textBox1.Text) && !Catch.cNullTB(textBox2.Text) && !Catch.cNullTB(textBox3.Text) && !Catch.cNullTB(textBox4.Text) && !Catch.cNullTB(textBox6.Text) && !Catch.cNullTB(textBox7.Text) && !Catch.cNullTB(textBox9.Text) && !Catch.cNullTB(textBox10.Text))
            {
                    BUSHocSinh.updateHS(hs);
                BUSHocSinh.updateDDHS1(MaHS,HoTen);
                BUSHocSinh.updateChiaHS1(MaHS, HoTen);
                BUSHocSinh.updateHPHS(MaHS, HoTen);
                dataGridView1.DataSource = BUSHocSinh.loadHS();
                    XoaText();
                    Count();

            }
            else
            {

                MessageBox.Show("Bạn chưa nhập vào đủ dữ liệu xin vui lòng nhập lại.");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            HocPhi hp = new HocPhi(s, t);
            hp.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            SongaynghiHS nn = new SongaynghiHS(s, t);
            nn.ShowDialog();
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BUSHocSinh.loadNV1(textBox12.Text, textBox12.Text);
        }
    }
}
