﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class HocPhi : Form
    {
        public HocPhi()
        {
            InitializeComponent();
        }

        public HocPhi(string s, string t):this()
        {
            label2.Text = t;
            label4.Text = s;
        }
        private void HocPhi_Load(object sender, EventArgs e)
        {
            Load1();
        }
        private void Load1()
        {
            string t = label4.Text;
            dataGridView1.DataSource = BUSHocPhi.loadHP(t);
        }
        private void xoa()
        {
            dateTimePicker1.ResetText();
            textBox1.Clear();
            dateTimePicker2.ResetText();
            dateTimePicker3.ResetText();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string ma = label4.Text;
            string ten = label2.Text;
            string Ngaydonghp = dateTimePicker1.Text;
            string hp = textBox1.Text;
            string Ngaybd = dateTimePicker2.Text;
            string Ngaykt = dateTimePicker3.Text;
            HocPhi1 hp1= new HocPhi1(ma, ten,Ngaydonghp,hp, Ngaybd, Ngaykt);
            BUSHocPhi.addHP(hp1);
            Load1();
            xoa();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ma = label4.Text;
            BUSHocPhi.deleteHP(ma);
            Load1();
            xoa();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
