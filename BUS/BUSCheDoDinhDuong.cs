﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUSCheDoDinhDuong
    {
        public static DataTable loadCDDD()
        {
            return DALCheDoDinhDuong.getData();
        }
        public static void addCDDD(CheDoDinhDuong cddd)
        {
            DALCheDoDinhDuong.insertCDDD(cddd);
        }
        public static bool cPrimaryKey(string _cPrim)
        {
            return DALCheDoDinhDuong.KiemTraKhoaChinh(_cPrim);
        }
        public static void deleteCDDD(string n)
        {
            DALCheDoDinhDuong.DeleteCDDD(n);
        }
    }
}
