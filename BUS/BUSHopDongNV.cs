﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUSHopDongNV
    {
        public static DataTable loadHD(string text)
        {
            return DALHopDongNV.getData(text);
        }
        public static void addLuong(HopDongNV1 hd)
        {
            DALHopDongNV.insertHD(hd);
        }
        public static void deleteNV(string n)
        {
            DALHopDongNV.DeleteHD(n);
        }
    }
}
