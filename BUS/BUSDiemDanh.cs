﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUSDiemDanh
    {
        public static DataTable loadNV2(string text1, string text2)
        {
            return DALDiemDanhGV.getData2(text1, text2);
        }
        public static DataTable loadNV1(string text1, string text2, string text3, string text4)
        {
            return DALDiemDanhGV.getData1(text1, text2, text3, text4);
        }
        public static void Vang(DiemDanhGV nv)
        {
            DALDiemDanhGV.insertNV(nv);
        }
        public static void deleteNV(string n,string m)
        {
            DALDiemDanhGV.DeleteNV(n,m);
        }
        public static bool cPrimaryKey(string _cPrim, string _textBox1)
        {
            return DALDiemDanhGV.KiemTraKhoaChinh(_cPrim,_textBox1);
        }

        public static DataTable loadNV3(string text1, string text2)
        {
            return DALDiemDanhHS.getData2(text1, text2);
        }
        public static DataTable loadNV4(string text1, string text2, string text3, string text4)
        {
            return DALDiemDanhHS.getData1(text1, text2, text3, text4);
        }
        public static void Vang1(DiemDanhHS nv)
        {
            DALDiemDanhHS.insertNV(nv);
        }
        public static void deleteNV1(string n,string m)
        {
            DALDiemDanhHS.DeleteNV(n,m);
        }
        public static bool cPrimaryKey1(string _cPrim, string _textBox1)
        {
            return DALDiemDanhHS.KiemTraKhoaChinh(_cPrim,_textBox1);
        }
    }
}
