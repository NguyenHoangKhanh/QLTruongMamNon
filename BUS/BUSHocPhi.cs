﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
   public class BUSHocPhi
    {
        public static DataTable loadHP(string text)
        {
            return DALHocPhi.getData(text);
        }
        public static void addHP(HocPhi1 hp)
        {
            DALHocPhi.insertHP(hp);
        }
        public static void deleteHP(string n)
        {
            DALHocPhi.DeleteHP(n);
        }
    }
}
