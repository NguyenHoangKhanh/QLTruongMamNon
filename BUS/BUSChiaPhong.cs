﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
   public class BUSChiaPhong
    {
        public static DataTable loadNV1()
        {
            return DALChiaPhong.getData1();
        }
        
        public static DataTable loadNV2(string text1,string text2)
        {
            return DALChiaPhong.getData2(text1,text2);
        }
        public static DataTable loadNV3(string text1, string text2)
        {
            return DALChiaPhong.getData3(text1, text2);
        }
        public static void addNV(ChiaPhong1 nv)
        {
            DALChiaPhong.insertNV(nv);
        }
        public static void deleteNV(string n)
        {
            DALChiaPhong.DeleteNV(n);
        }
        public static bool cPrimaryKey(string _cPrim, string _textBox1)
        {
            return DALChiaPhong.KiemTraKhoaChinh(_cPrim,  _textBox1);
        }

        public static DataTable loadNV4()
        {
            return DALChiaHS.getData1();
        }
        public static DataTable loadNV5(string text1, string text2)
        {
            return DALChiaHS.getData2(text1, text2);
        }
        public static DataTable loadNV6(string text1, string text2)
        {
            return DALChiaHS.getData3(text1, text2);
        }
        public static void addHS(ChiaPhong2 nv)
        {
            DALChiaHS.insertHS(nv);
        }
        public static void deleteHS(string n)
        {
            DALChiaHS.DeleteHS(n);
        }
        public static bool cPrimaryKey1(string _cPrim, string _textBox1)
        {
            return DALChiaHS.KiemTraKhoaChinh(_cPrim,  _textBox1);
        }
    }
}
