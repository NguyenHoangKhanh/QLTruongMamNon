﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BUS
{
    public class BUSHocSinh
    {
        public static DataTable loadHS()
        {
            return HocSinhh.getData();
        }
        public static void addHS(HocSinh1 hs)
        {
            HocSinhh.insertHS(hs);
        }
        public static void updateHS(HocSinh1 hs)
        {
            HocSinhh.updateHS(hs);
        }
        public static void updateDDHS1(string n,string m)
        {
            HocSinhh.updateDDHS1(n,m);
        }
        public static void updateChiaHS1(string n, string m)
        {
            HocSinhh.updateChiaHS1(n, m);
        }
        public static void updateHPHS(string n, string m)
        {
            HocSinhh.updateHPHS(n, m);
        }
        public static bool cPrimaryKey(string _cPrim)
        {
            return HocSinhh.KiemTraKhoaChinh(_cPrim);
        }
        public static void deleteHS(string n)
        {
            HocSinhh.DeleteHS(n);
        }
        public static DataTable loadNV1(string text1, string text2)
        {
            return HocSinhh.getData1(text1, text2);
        }
    }
}
