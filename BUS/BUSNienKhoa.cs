﻿using DTO;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUSNienKhoa
    {
        public static DataTable loadData()
        {
            return NamHoc.getData();
        }
        public static void addNam(NienKhoa nk)
        {
            NamHoc.insertNam(nk);
        }
        public static bool cPrimaryKey(string _cPrim)
        {
            return NamHoc.KiemTraKhoaChinh(_cPrim);
        }
        public static void deleteNam(string n)
        {
             NamHoc.DeleteNam(n);
        }
        public static DataTable loadData1(string nam)
        {
            return Khoi1.getData(nam);
        }
        public static bool cPrimaryKey1(string _cPrim)
        {
            return Khoi1.KiemTraKhoaChinh1(_cPrim);
        }
        public static void addKhoi(Khoi k)
        {
            Khoi1.insertKhoi(k);
        }
        public static void deleteKhoi(string text,string text1)
        {
            Khoi1.DeleteKhoi(text,text1);
        }
        public static void updateeKhoi(string text, string text1)
        {
            Khoi1.updateKhoi(text, text1);
        }
        public static void updateeCDDD(string text, string text1)
        {
            Khoi1.updateCDDD(text, text1);
        }
        public static void updateeDDNV(string text, string text1)
        {
            Khoi1.updateDDNV(text, text1);
        }
        public static void updateeChiaNV(string text, string text1)
        {
            Khoi1.updateKhoi(text, text1);
        }
        public static void updateeDDHS(string text, string text1)
        {
            Khoi1.updateDDHS(text, text1);
        }
        public static void updateeChiaHS(string text, string text1)
        {
            Khoi1.updateChiaHS(text, text1);
        }





    }
}
