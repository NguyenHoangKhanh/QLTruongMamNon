﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
   public class BUSSoNgayNghi
    {
        public static DataTable loadSNN(string text)
        {
            return DALSoNgayNghi.getData(text);
        }
        public static void updatee(string text1, string text2, string text3, string text4)
        {
             DALSoNgayNghi.updatePhong(text1,text2,text3,text4);
        }
        public static string LoadLopp(string n)
        {
            return DALSoNgayNghi.LoadLopGV(n);
        }

    }
}
