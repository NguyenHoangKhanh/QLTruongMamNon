﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
   public class BUSLuongcoban
    {
        public static DataTable loadLuong(string text)
        {
            return DALLuongcoban.getData(text);
        }
        public static void addLuong(LuongCoBan1 lcb)
        {
            DALLuongcoban.insertLuong(lcb);
        }
        public static void deleteNV(string n,string m)
        {
            DALLuongcoban.DeleteLuong(n,m);
        }
    }
}
