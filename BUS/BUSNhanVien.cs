﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUSNhanVien
    {
        public static DataTable loadNV()
        {
            return NV.getData();
        }
        public static void addNV(NhanVien nv )
        {
            NV.insertNV(nv);
        }
        public static bool cPrimaryKey(string _cPrim)
        {
            return NV.KiemTraKhoaChinh(_cPrim);
        }
        public static void deleteNV(string n)
        {
            NV.DeleteNV(n);
        }
        public static void updateNV(NhanVien n)
        {
            NV.updateNV(n);
        }
        public static void updateHD(string n,string m)
        {
            NV.UpdateHD(n,m);
        }
        public static void updateDDGV1(string n, string m)
        {
            NV.updateDDGV1(n, m);
        }
        public static void updateChiaNV1(string n, string m)
        {
            NV.UpdateChiaNV1(n, m);
        }
        public static void updateLCB(string n, string m)
        {
            NV.updateLuongcoban(n, m);
        }
        public static DataTable loadNV1(string text1, string text2)
        {
            return NV.getData1(text1, text2);
        }
    }
}
