﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class DALHocPhi
    {
        public static DataTable getData(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadHP", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertHP(HocPhi1 hp)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertHP", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenHS", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Ngaydonghocphi", SqlDbType.Text);
            cmd.Parameters.Add("@Hocphi", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Ngaybatdauhoc", SqlDbType.Text);
            cmd.Parameters.Add("@Ngayketthuc", SqlDbType.Text);
            cmd.Parameters["@MaHS"].Value = hp.MaHS;
            cmd.Parameters["@TenHS"].Value = hp.TenHS;
            cmd.Parameters["@Ngaydonghocphi"].Value = hp.Ngaydonghp;
            cmd.Parameters["@Hocphi"].Value = hp.Hp;
            cmd.Parameters["@Ngaybatdauhoc"].Value = hp.Ngaybatdauhoc;
            cmd.Parameters["@Ngayketthuc"].Value = hp.NgayKetThuc;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteHP(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("DeleteHP", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
    }
}
