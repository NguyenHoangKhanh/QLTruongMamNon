﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALHopDongNV
    {
        public static DataTable getData(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadHopDong", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertHD(HopDongNV1 hd)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertHD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgayBatDau", SqlDbType.Text);
            cmd.Parameters.Add("@NgayKetThuc", SqlDbType.Text);
            cmd.Parameters["@MaNV"].Value = hd.MaNV;
            cmd.Parameters["@TenNV"].Value = hd.TenNV;
            cmd.Parameters["@NgayBatDau"].Value = hd.NgayBatDau;
            cmd.Parameters["@NgayKetThuc"].Value = hd.NgayKetThuc;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteHD(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("DeleteHD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
    }
}
