﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALCheDoDinhDuong
    {
        public static DataTable getData()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadCDDD", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static bool KiemTraKhoaChinh(string _textBox)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkCDDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaLop"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void insertCDDD(CheDoDinhDuong cddd )
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertCDDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Tuan", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Ngay", SqlDbType.Text);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@BuoiSang", SqlDbType.NVarChar, 200);
            cmd.Parameters.Add("@BuoiTrua", SqlDbType.NVarChar, 200);
            cmd.Parameters.Add("@BuoiChieu", SqlDbType.NVarChar, 200);
            cmd.Parameters.Add("@TongTien", SqlDbType.NVarChar, 100);
            cmd.Parameters["@NamHoc"].Value = cddd.NamHoc;
            cmd.Parameters["@Tuan"].Value = cddd.Tuan;
            cmd.Parameters["@Ngay"].Value = cddd.Ngay;
            cmd.Parameters["@Lop"].Value = cddd.Lop;
            cmd.Parameters["@MaLop"].Value = cddd.MaLop;
            cmd.Parameters["@BuoiSang"].Value = cddd.BuoiSang;
            cmd.Parameters["@BuoiTrua"].Value = cddd.BuoiTrua;
            cmd.Parameters["@BuoiChieu"].Value = cddd.BuoiChieu;
            cmd.Parameters["@TongTien"].Value = cddd.TongTien;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteCDDD(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteCDDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
    }
}
