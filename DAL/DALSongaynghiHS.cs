﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class DALSongaynghiHS
    {
        public static DataTable getData(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadHSVang", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void updatePhong(string text1, string text2, string text3, string text4)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updatePhongHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 5);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = text1;
            cmd.Parameters["@NamHoc"].Value = text2;
            cmd.Parameters["@Lop"].Value = text3;
            cmd.Parameters["@MaLop"].Value = text4;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static string LoadLop(string text)
        {
            string s = " ";
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadLop", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                s = (string)dr["Lop"];
            }
            return s;

        }
    }
}
