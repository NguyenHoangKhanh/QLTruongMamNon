﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DTO;

namespace DAL
{
    public class DALL
    {
        public static SqlConnection HamKetNoi()
        {
            SqlConnection Conn = new SqlConnection(@"Data Source=DESKTOP-RM7RC46\SQLEXPRESS;Initial Catalog=QLMN;Integrated Security=True");
            return Conn;
        }
    }
    public class NamHoc
    {
        public static DataTable getData()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("loadData", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static bool KiemTraKhoaChinh(string _textBox)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();         
            SqlCommand cmd = new SqlCommand("checkData", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void insertNam(NienKhoa nk)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Khoa", SqlDbType.NVarChar, 20);
            cmd.Parameters.Add("@NgayKhaiGiang", SqlDbType.Text);
            cmd.Parameters.Add("@NgayKetThuc", SqlDbType.Text);
            cmd.Parameters["@Namhoc"].Value = nk.NamH;
            cmd.Parameters["@Khoa"].Value = nk.Khoa;
            cmd.Parameters["@NgayKhaiGiang"].Value = nk.NgayKhaiG;
            cmd.Parameters["@NgayKetThuc"].Value = nk.NgayKetT;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteNam(string nk)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteData", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Namhoc"].Value = nk;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        
    }
    public class Khoi1
    {
        public static DataTable getData(string nam)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadData01", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            command.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            command.Parameters["@Namhoc"].Value = nam;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static bool KiemTraKhoaChinh1(string _textBox)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkData1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaLop"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void insertKhoi(Khoi k)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Namhoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = k.MaLop;
            cmd.Parameters["@Namhoc"].Value = k.NamHoc;
            cmd.Parameters["@TenLop"].Value = k.TenLop;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteKhoi(string text,string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@NamHoc"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateKhoi(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateKhoi", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateCDDD(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateCDDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateDDNV(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateDDNV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateChiaNV(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateChiaNV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateDDHS(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateDiemDanhHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateChiaHS(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateChiaHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TenLop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaLop"].Value = text;
            cmd.Parameters["@TenLop"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }

    }
}
