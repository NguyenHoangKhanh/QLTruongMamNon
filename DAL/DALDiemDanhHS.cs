﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class DALDiemDanhHS
    {
        public static DataTable getData2(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadMaTenHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = text1;
            cmd.Parameters["@Lop"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static DataTable getData1(string text1, string text2, string text3, string text4)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Tuan", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@Tuan"].Value = text1;
            cmd.Parameters["@Thu"].Value = text2;
            cmd.Parameters["@NamHoc"].Value = text3;
            cmd.Parameters["@Lop"].Value = text4;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertNV(DiemDanhHS hs)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertDDHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Tuan", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenHS", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Tuan"].Value = hs.Tuan;
            cmd.Parameters["@Thu"].Value = hs.Thu;
            cmd.Parameters["@NamHoc"].Value = hs.NamHoc;
            cmd.Parameters["@Lop"].Value = hs.Lop;
            cmd.Parameters["@MaHS"].Value = hs.MaHS;
            cmd.Parameters["@TenHS"].Value = hs.TenHS;
            cmd.Parameters["@MaLop"].Value = hs.MaLop;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteNV(string text,string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteDDHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.Parameters["@Thu"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static bool KiemTraKhoaChinh(string _textBox, string _textBox1)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkDDHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters["@MaHS"].Value = _textBox;
            cmd.Parameters["@Thu"].Value = _textBox1;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
    }
}
