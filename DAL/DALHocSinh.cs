﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class HocSinhh
    {
        public static DataTable getData()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadHS", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static bool KiemTraKhoaChinh(string _textBox)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkMaHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void insertHS(HocSinh1 hs)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTen", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@NgaySinh", SqlDbType.Text);
            cmd.Parameters.Add("@SDTLL", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTenBo", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgheNghiepBo1", SqlDbType.NVarChar,100);
            cmd.Parameters.Add("@EmailBo", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@HoTenMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgheNghiepMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@EmailMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = hs.MaHS;
            cmd.Parameters["@HoTen"].Value = hs.HoTen;
            cmd.Parameters["@GioiTinh"].Value = hs.GioiTinh;
            cmd.Parameters["@NgaySinh"].Value = hs.NgaySinh;
            cmd.Parameters["@SDTLL"].Value = hs.SDT;
            cmd.Parameters["@HoTenBo"].Value = hs.HoTenBo;
            cmd.Parameters["@NgheNghiepBo1"].Value = hs.NgheNghiepBo1;
            cmd.Parameters["@EmailBo"].Value = hs.EmailBo;
            cmd.Parameters["@HoTenMe"].Value = hs.HoTenMe;
            cmd.Parameters["@NgheNghiepMe"].Value = hs.NgheNghiepMe;
            cmd.Parameters["@EmailMe"].Value = hs.EmailMe;
            cmd.Parameters["@DiaChi"].Value = hs.DiaChi;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateHS(HocSinh1 hs)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTen", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@NgaySinh", SqlDbType.Text);
            cmd.Parameters.Add("@SDTLL", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTenBo", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgheNghiepBo1", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@EmailBo", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@HoTenMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgheNghiepMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@EmailMe", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = hs.MaHS;
            cmd.Parameters["@HoTen"].Value = hs.HoTen;
            cmd.Parameters["@GioiTinh"].Value = hs.GioiTinh;
            cmd.Parameters["@NgaySinh"].Value = hs.NgaySinh;
            cmd.Parameters["@SDTLL"].Value = hs.SDT;
            cmd.Parameters["@HoTenBo"].Value = hs.HoTenBo;
            cmd.Parameters["@NgheNghiepBo1"].Value = hs.NgheNghiepBo1;
            cmd.Parameters["@EmailBo"].Value = hs.EmailBo;
            cmd.Parameters["@HoTenMe"].Value = hs.HoTenMe;
            cmd.Parameters["@NgheNghiepMe"].Value = hs.NgheNghiepMe;
            cmd.Parameters["@EmailMe"].Value = hs.EmailMe;
            cmd.Parameters["@DiaChi"].Value = hs.DiaChi;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateDDHS1(string n,string m)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateDDHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTen", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = n;
            cmd.Parameters["@HoTen"].Value = m;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateChiaHS1(string n, string m)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateChiaHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTen", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = n;
            cmd.Parameters["@HoTen"].Value = m;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateHPHS(string n, string m)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateHPHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@HoTen", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = n;
            cmd.Parameters["@HoTen"].Value = m;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteHS(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static DataTable getData1(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("TimKiem3", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenHS", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = text1;
            cmd.Parameters["@TenHS"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }

    }
}
