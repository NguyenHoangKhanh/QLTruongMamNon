﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALDN
    {
        public static bool KiemTraKhoaChinh1(string _textBox, string _textBox1)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkDN1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ID", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MK", SqlDbType.NVarChar, 50);
            cmd.Parameters["@ID"].Value = _textBox;
            cmd.Parameters["@MK"].Value = _textBox1;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void updateDN(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateDN", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@ID", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@MK", SqlDbType.NVarChar, 100);
            cmd.Parameters["@ID"].Value = text1;
            cmd.Parameters["@MK"].Value = text2;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }

    }
}
