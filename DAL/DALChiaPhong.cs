﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALChiaPhong
    {
        public static DataTable getData1()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadMaTenNV", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static DataTable getData2(string text1,string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadMaTenNV1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = text1;
            cmd.Parameters["@Lop"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        

        public static void insertNV(ChiaPhong1 nv)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertChiaNV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar,50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@NamHoc"].Value = nv.NamHoc;
            cmd.Parameters["@Lop"].Value = nv.Lop;
            cmd.Parameters["@MaNV"].Value = nv.MaNV;
            cmd.Parameters["@TenNV"].Value = nv.TenNV;
            cmd.Parameters["@MaLop"].Value = nv.MaLop;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteNV(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteChiaNV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
      
        public static bool KiemTraKhoaChinh(string _textBox,string _textBox1)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkMaNV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaNV"].Value = _textBox;
            cmd.Parameters["@NamHoc"].Value = _textBox1;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static DataTable getData3(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("TimKiem", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaNV"].Value = text1;
            cmd.Parameters["@TenNV"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
    }
    public class DALChiaHS
    {
        public static DataTable getData1()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadMaTenHS", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static DataTable getData2(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadMaTenHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = text1;
            cmd.Parameters["@Lop"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertHS(ChiaPhong2 nv)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertChiaHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@MaHS", SqlDbType.Text);
            cmd.Parameters.Add("@TenHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@NamHoc"].Value = nv.NamHoc;
            cmd.Parameters["@Lop"].Value = nv.Lop;
            cmd.Parameters["@MaHS"].Value = nv.MaHS;
            cmd.Parameters["@TenHS"].Value = nv.TenHS;
            cmd.Parameters["@MaLop"].Value = nv.MaLop;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteHS(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteChiaHS", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaHS"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static bool KiemTraKhoaChinh(string _textBox,string _textBox1)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkMaHS1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters["@NamHoc"].Value = _textBox1;
            cmd.Parameters["@MaHS"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();     
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static DataTable getData3(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("TimKiem1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaHS", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenHS", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaHS"].Value = text1;
            cmd.Parameters["@TenHS"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
    }
}
