﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class DALDiemDanhGV
    {
        public static DataTable getData2(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadMaTenNV1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@NamHoc"].Value = text1;
            cmd.Parameters["@Lop"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static DataTable getData1(string text1, string text2, string text3, string text4)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Tuan", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters["@Tuan"].Value = text1;
            cmd.Parameters["@Thu"].Value = text2;
            cmd.Parameters["@NamHoc"].Value = text3;
            cmd.Parameters["@Lop"].Value = text4;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertNV(DiemDanhGV gv)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@Tuan", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters.Add("@NamHoc", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Lop", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@MaLop", SqlDbType.NVarChar, 100);
            cmd.Parameters["@Tuan"].Value = gv.Tuan;
            cmd.Parameters["@Thu"].Value = gv.Thu;
            cmd.Parameters["@NamHoc"].Value = gv.NamHoc;
            cmd.Parameters["@Lop"].Value = gv.Lop;
            cmd.Parameters["@MaGV"].Value = gv.MaGV;
            cmd.Parameters["@TenGV"].Value = gv.TenGV;
            cmd.Parameters["@MaLop"].Value = gv.MaLop;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteNV(string text,string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters["@MaGV"].Value = text;
            cmd.Parameters["@Thu"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static bool KiemTraKhoaChinh(string _textBox, string _textBox1)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkDD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@Thu", SqlDbType.Text);
            cmd.Parameters["@MaGV"].Value = _textBox;
            cmd.Parameters["@Thu"].Value = _textBox1;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
    }
}
