﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALNhanVien
    { }
    public class NV { 
        public static DataTable getData()
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand command = new SqlCommand("LoadGV", Conn);
            command.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static bool KiemTraKhoaChinh(string _textBox)
        {
            bool check = false;
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("checkMaGV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = _textBox;
            Conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                check = true;
            }
            return check;
        }
        public static void insertNV(NhanVien nv)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertGV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgaySinh", SqlDbType.Text);
            cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar,50);
            cmd.Parameters.Add("@BangCap", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar, 200);
            cmd.Parameters.Add("@DienThoai", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TinhTrang", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = nv.MaNV;
            cmd.Parameters["@TenGV"].Value = nv.TenNV;
            cmd.Parameters["@NgaySinh"].Value = nv.NgaySinh;
            cmd.Parameters["@GioiTinh"].Value = nv.GioiTinh;
            cmd.Parameters["@BangCap"].Value = nv.BangCap;
            cmd.Parameters["@DiaChi"].Value = nv.DiaChi;
            cmd.Parameters["@DienThoai"].Value = nv.DT;
            cmd.Parameters["@TinhTrang"].Value = nv.TinhTrang;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteNV(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("deleteGV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaGV"].Value = text;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void UpdateHD(string text,string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateHD", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = text;
            cmd.Parameters["@TenGV"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void UpdateChiaNV1(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateChiaNV1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = text;
            cmd.Parameters["@TenGV"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateDDGV1(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateDDGV1", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = text;
            cmd.Parameters["@TenGV"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateLuongcoban(string text, string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateLuongcoban", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.Parameters["@TenNV"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void updateNV(NhanVien nv)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("updateGV", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaGV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenGV", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgaySinh", SqlDbType.Text);
            cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@BangCap", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar, 200);
            cmd.Parameters.Add("@DienThoai", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@TinhTrang", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaGV"].Value = nv.MaNV;
            cmd.Parameters["@TenGV"].Value = nv.TenNV;
            cmd.Parameters["@NgaySinh"].Value = nv.NgaySinh;
            cmd.Parameters["@GioiTinh"].Value = nv.GioiTinh;
            cmd.Parameters["@BangCap"].Value = nv.BangCap;
            cmd.Parameters["@DiaChi"].Value = nv.DiaChi;
            cmd.Parameters["@DienThoai"].Value = nv.DT;
            cmd.Parameters["@TinhTrang"].Value = nv.TinhTrang;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static DataTable getData1(string text1, string text2)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("TimKiem2", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaNV"].Value = text1;
            cmd.Parameters["@TenNV"].Value = text2;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
    }
}
