﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class DALLuongcoban
    {
        public static DataTable getData(string text)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("LoadLuong", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        public static void insertLuong(LuongCoBan1 lcb)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("insertLuong", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@TenNV", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@NgayBatDau", SqlDbType.Text);
            cmd.Parameters.Add("@Luongcoban", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@Luongbhxh", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@phucap1", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@phucap2", SqlDbType.NVarChar, 100);
            cmd.Parameters.Add("@phucap3", SqlDbType.NVarChar, 100);
            cmd.Parameters["@MaNV"].Value = lcb.MaNV;
            cmd.Parameters["@TenNV"].Value = lcb.TenNV;
            cmd.Parameters["@NgayBatDau"].Value = lcb.NgayBatDau;
            cmd.Parameters["@Luongcoban"].Value = lcb.Luongcoban;
            cmd.Parameters["@Luongbhxh"].Value = lcb.LuongBHXH;
            cmd.Parameters["@phucap1"].Value = lcb.Phucap1;
            cmd.Parameters["@phucap2"].Value = lcb.Phucap2;
            cmd.Parameters["@phucap3"].Value = lcb.Phucap3;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
        public static void DeleteLuong(string text,string text1)
        {
            SqlConnection Conn = DALL.HamKetNoi();
            SqlCommand cmd = new SqlCommand("DeleteLuong", Conn);
            cmd.CommandType = CommandType.StoredProcedure;
            Conn.Open();
            cmd.Parameters.Add("@MaNV", SqlDbType.NVarChar, 50);
            cmd.Parameters.Add("@NgayBatDau", SqlDbType.Text);
            cmd.Parameters["@MaNV"].Value = text;
            cmd.Parameters["@NgayBatDau"].Value = text1;
            cmd.ExecuteNonQuery();
            Conn.Close();
        }
    }
}
