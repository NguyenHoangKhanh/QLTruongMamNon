﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class HocSinh1
    {
        private string _MaHS;
        private string _HoTen;
        private string _GioiTinh;
        private string _NgaySinh;
        private string _SDT;
        private string _HoTenBo;
        private string _NgheNghiepBo;
        private string _EmailBo;
        private string _HoTenMe;
        private string _NgheNghiepMe;
        private string _EmailMe;
        private string _DiaChi;

        public string MaHS
        {
            get
            {
                return _MaHS;
            }

            set
            {
                _MaHS = value;
            }
        }

        public string HoTen
        {
            get
            {
                return _HoTen;
            }

            set
            {
                _HoTen = value;
            }
        }

        public string GioiTinh
        {
            get
            {
                return _GioiTinh;
            }

            set
            {
                _GioiTinh = value;
            }
        }

        public string NgaySinh
        {
            get
            {
                return _NgaySinh;
            }

            set
            {
                _NgaySinh = value;
            }
        }

        public string SDT
        {
            get
            {
                return _SDT;
            }

            set
            {
                _SDT = value;
            }
        }

        public string HoTenBo
        {
            get
            {
                return _HoTenBo;
            }

            set
            {
                _HoTenBo = value;
            }
        }

        public string NgheNghiepBo1
        {
            get
            {
                return _NgheNghiepBo;
            }

            set
            {
                _NgheNghiepBo = value;
            }
        }

        public string EmailBo
        {
            get
            {
                return _EmailBo;
            }

            set
            {
                _EmailBo = value;
            }
        }

        public string HoTenMe
        {
            get
            {
                return _HoTenMe;
            }

            set
            {
                _HoTenMe = value;
            }
        }

        public string NgheNghiepMe
        {
            get
            {
                return _NgheNghiepMe;
            }

            set
            {
                _NgheNghiepMe = value;
            }
        }

        public string EmailMe
        {
            get
            {
                return _EmailMe;
            }

            set
            {
                _EmailMe = value;
            }
        }

        public string DiaChi
        {
            get
            {
                return _DiaChi;
            }

            set
            {
                _DiaChi = value;
            }
        }
        public HocSinh1(string mahs,string hoten,string gioitinh,string ngaysinh,string sdt,string hotenbo,string nghenghiepbo,string emailbo,string hotenme,string nghenghiepme,string emailme,string diachi)
        {
            _MaHS = mahs;
            _HoTen = hoten;
            _GioiTinh = gioitinh;
            _NgaySinh = ngaysinh;
            _SDT = sdt;
            _HoTenBo = hotenbo;
            _NgheNghiepBo = nghenghiepbo;
            _EmailBo = emailbo;
            _HoTenMe = hotenme;
            _NgheNghiepMe = nghenghiepme;
            _EmailMe = emailme;
            _DiaChi = diachi;
        }
    }
}
