﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class HocPhi1
    {
        private string _MaHS;
        private string _TenHS;
        private string _Ngaydonghp;
        private string _Hp;
        private string _Ngaybatdauhoc;
        private string _NgayKetThuc;

        public string MaHS
        {
            get
            {
                return _MaHS;
            }

            set
            {
                _MaHS = value;
            }
        }

        public string TenHS
        {
            get
            {
                return _TenHS;
            }

            set
            {
                _TenHS = value;
            }
        }

        public string Ngaydonghp
        {
            get
            {
                return _Ngaydonghp;
            }

            set
            {
                _Ngaydonghp = value;
            }
        }

        public string Hp
        {
            get
            {
                return _Hp;
            }

            set
            {
                _Hp = value;
            }
        }

        public string Ngaybatdauhoc
        {
            get
            {
                return _Ngaybatdauhoc;
            }

            set
            {
                _Ngaybatdauhoc = value;
            }
        }

        public string NgayKetThuc
        {
            get
            {
                return _NgayKetThuc;
            }

            set
            {
                _NgayKetThuc = value;
            }
        }
        public HocPhi1(string mahs,string tenhs,string ngaydonghp,string hp,string ngaybdhoc,string ngayketthuc)
        {
            _MaHS = mahs;
            _TenHS = tenhs;
            _Ngaydonghp = ngaydonghp;
            _Hp = hp;
            _Ngaybatdauhoc = ngaybdhoc;
            _NgayKetThuc = ngayketthuc;
        }
    }
}
