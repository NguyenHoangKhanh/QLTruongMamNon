﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class HopDongNV1
    {
        private string _MaNV;
        private string _TenNV;
        private string _NgayBatDau;
        private string _NgayKetThuc;

        public string MaNV
        {
            get
            {
                return _MaNV;
            }

            set
            {
                _MaNV = value;
            }
        }

        public string TenNV
        {
            get
            {
                return _TenNV;
            }

            set
            {
                _TenNV = value;
            }
        }

        public string NgayBatDau
        {
            get
            {
                return _NgayBatDau;
            }

            set
            {
                _NgayBatDau = value;
            }
        }

        public string NgayKetThuc
        {
            get
            {
                return _NgayKetThuc;
            }

            set
            {
                _NgayKetThuc = value;
            }
        }
        public HopDongNV1(string manv,string tennv,string nbd,string nkt)
        {
            _MaNV = manv;
            _TenNV = tennv;
            _NgayBatDau = nbd;
            _NgayKetThuc = nkt;
        }
    }
}
