﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class ChiaPhong2
    {
        private string _NamHoc;
        private string _Lop;
        private string _MaHS;
        private string _TenHS;
        private string _MaLop;
        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public string Lop
        {
            get
            {
                return _Lop;
            }

            set
            {
                _Lop = value;
            }
        }

        public string MaHS
        {
            get
            {
                return _MaHS;
            }

            set
            {
                _MaHS = value;
            }
        }

        public string TenHS
        {
            get
            {
                return _TenHS;
            }

            set
            {
                _TenHS = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public ChiaPhong2(string namhoc, string lop, string mahs, string tenhs,string malop)
        {
            _NamHoc = namhoc;
            _Lop = lop;
            _MaHS = mahs;
            _TenHS = tenhs;
            _MaLop = malop;
        }
    }
}
