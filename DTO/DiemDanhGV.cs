﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class DiemDanhGV
    {
        private string _Tuan;
        private string _Thu;
        private string _MaGV;
        private string _TenGV;
        private string _NamHoc;
        private string _Lop;
        private string _MaLop;
        public string Tuan
        {
            get
            {
                return _Tuan;
            }

            set
            {
                _Tuan = value;
            }
        }


        public string MaGV
        {
            get
            {
                return _MaGV;
            }

            set
            {
                _MaGV = value;
            }
        }

        public string TenGV
        {
            get
            {
                return _TenGV;
            }

            set
            {
                _TenGV = value;
            }
        }

        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public string Lop
        {
            get
            {
                return _Lop;
            }

            set
            {
                _Lop = value;
            }
        }

        public string Thu
        {
            get
            {
                return _Thu;
            }

            set
            {
                _Thu = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public DiemDanhGV(string tuan,string thu,string magv,string tengv,string namhoc,string lop,string malop)
        {
            _Tuan = tuan;
            Thu = thu;
            _MaGV = magv;
            _TenGV = tengv;
            _NamHoc = namhoc;
            _Lop = lop;
            _MaLop = malop;
        }
    }
}
