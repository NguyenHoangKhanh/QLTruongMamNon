﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class Khoi
    {
        private string _NamHoc;
        private string _TenLop;
        private string _MaLop;
        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public string TenLop
        {
            get
            {
                return _TenLop;
            }

            set
            {
                _TenLop = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public Khoi(string malop,string khoi, string namhoc)
        {
            _MaLop = malop;
            _NamHoc = namhoc;
            _TenLop = khoi;
            
        }
    }
}
