﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LuongCoBan1
    {
        private string _MaNV;
        private string _TenNV;
        private string _NgayBatDau;
        private string _Luongcoban;
        private string _LuongBHXH;
        private string _Phucap1;
        private string _Phucap2;
        private string _Phucap3;

        public string MaNV
        {
            get
            {
                return _MaNV;
            }

            set
            {
                _MaNV = value;
            }
        }

        public string TenNV
        {
            get
            {
                return _TenNV;
            }

            set
            {
                _TenNV = value;
            }
        }

        public string NgayBatDau
        {
            get
            {
                return _NgayBatDau;
            }

            set
            {
                _NgayBatDau = value;
            }
        }

        public string Luongcoban
        {
            get
            {
                return _Luongcoban;
            }

            set
            {
                _Luongcoban = value;
            }
        }

        public string LuongBHXH
        {
            get
            {
                return _LuongBHXH;
            }

            set
            {
                _LuongBHXH = value;
            }
        }

        public string Phucap1
        {
            get
            {
                return _Phucap1;
            }

            set
            {
                _Phucap1 = value;
            }
        }

        public string Phucap2
        {
            get
            {
                return _Phucap2;
            }

            set
            {
                _Phucap2 = value;
            }
        }

        public string Phucap3
        {
            get
            {
                return _Phucap3;
            }

            set
            {
                _Phucap3 = value;
            }
        }
        public LuongCoBan1(string manv,string tennv,string ngaybatdau,string luongcoban,string luongbhxh,string phucap1,string phucap2,string phucap3)
        {
            _MaNV = manv;
            _TenNV = tennv;
            _NgayBatDau = ngaybatdau;
            _Luongcoban = luongcoban;
            _LuongBHXH = luongbhxh;
            _Phucap1 = phucap1;
            _Phucap2 = phucap2;
            _Phucap3 = phucap3;
        }
    }
}
