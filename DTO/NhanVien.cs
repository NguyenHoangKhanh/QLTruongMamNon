﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NhanVien
    {
        private string _MaNV;
        private string _TenNV;
        private string _NgaySinh;
        private string _GioiTinh;
        private string _BangCap;
        private string _DiaChi;
        private string _DT;
        private string _TinhTrang;

      

        public string NgaySinh
        {
            get
            {
                return _NgaySinh;
            }

            set
            {
                _NgaySinh = value;
            }
        }

        public string GioiTinh
        {
            get
            {
                return _GioiTinh;
            }

            set
            {
                _GioiTinh = value;
            }
        }

        public string BangCap
        {
            get
            {
                return _BangCap;
            }

            set
            {
                _BangCap = value;
            }
        }

        public string DiaChi
        {
            get
            {
                return _DiaChi;
            }

            set
            {
                _DiaChi = value;
            }
        }

        public string DT
        {
            get
            {
                return _DT;
            }

            set
            {
                _DT = value;
            }
        }

        public string TinhTrang
        {
            get
            {
                return _TinhTrang;
            }

            set
            {
                _TinhTrang = value;
            }
        }

        public string MaNV
        {
            get
            {
                return _MaNV;
            }

            set
            {
                _MaNV = value;
            }
        }

        public string TenNV
        {
            get
            {
                return _TenNV;
            }

            set
            {
                _TenNV = value;
            }
        }

        public NhanVien(string manv,string tennv,string ngaysinh,string gioitinh,string bangcap,string diachi,string dt,string tinhtrang )
        {
            _MaNV = manv;
            _TenNV = tennv;
            _NgaySinh = ngaysinh;
            _GioiTinh = gioitinh;
            _BangCap = bangcap;
            _DiaChi = diachi;
            _DT = dt;
            _TinhTrang = tinhtrang;
        }
    }
}
