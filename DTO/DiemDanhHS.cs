﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class DiemDanhHS
    {
        private string _Tuan;
        private string _Thu;
        private string _MaHS;
        private string _TenHS;
        private string _NamHoc;
        private string _Lop;
        private string _MaLop;

        public string Tuan
        {
            get
            {
                return _Tuan;
            }

            set
            {
                _Tuan = value;
            }
        }


       

        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public string Lop
        {
            get
            {
                return _Lop;
            }

            set
            {
                _Lop = value;
            }
        }

        public string Thu
        {
            get
            {
                return _Thu;
            }

            set
            {
                _Thu = value;
            }
        }

        public string MaHS
        {
            get
            {
                return _MaHS;
            }

            set
            {
                _MaHS = value;
            }
        }

        public string TenHS
        {
            get
            {
                return _TenHS;
            }

            set
            {
                _TenHS = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public DiemDanhHS(string tuan, string thu, string mahs, string tenhs, string namhoc, string lop,string malop)
        {
            _Tuan = tuan;
            Thu = thu;
            MaHS = mahs;
            TenHS = tenhs;
            _NamHoc = namhoc;
            _Lop = lop;
            _MaLop = malop;
        }
    }
}
