﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class DN
    {
        private string ID;
        private string MK;
        private string Cap;

        public string ID1
        {
            get
            {
                return ID;
            }

            set
            {
                ID = value;
            }
        }

        public string MK1
        {
            get
            {
                return MK;
            }

            set
            {
                MK = value;
            }
        }

        public string Cap1
        {
            get
            {
                return Cap;
            }

            set
            {
                Cap = value;
            }
        }
        public DN(string id,string mk,string cap)
        {
            ID = id;
            MK = mk;
            Cap = cap;
        }

    }
}
