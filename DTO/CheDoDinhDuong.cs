﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class CheDoDinhDuong
    {
        private string _NamHoc;
        private string _Tuan;
        private string _Ngay;
        private string _Lop;
        private string _MaLop;
        private string _BuoiSang;
        private string _BuoiTrua;
        private string _BuoiChieu;
        private string _TongTien;

        public string Tuan
        {
            get
            {
                return _Tuan;
            }

            set
            {
                _Tuan = value;
            }
        }

        public string Ngay
        {
            get
            {
                return _Ngay;
            }

            set
            {
                _Ngay = value;
            }
        }

        public string Lop
        {
            get
            {
                return _Lop;
            }

            set
            {
                _Lop = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public string BuoiSang
        {
            get
            {
                return _BuoiSang;
            }

            set
            {
                _BuoiSang = value;
            }
        }

        public string BuoiTrua
        {
            get
            {
                return _BuoiTrua;
            }

            set
            {
                _BuoiTrua = value;
            }
        }

        public string BuoiChieu
        {
            get
            {
                return _BuoiChieu;
            }

            set
            {
                _BuoiChieu = value;
            }
        }

        public string TongTien
        {
            get
            {
                return _TongTien;
            }

            set
            {
                _TongTien = value;
            }
        }

        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public CheDoDinhDuong(string namhoc,string tuan,string ngay,string lop,string malop,string buoisang,string buoitrua,string buoichieu,string tongtien)
        {
            _NamHoc = namhoc;
            _Tuan = tuan;
            _Ngay = ngay;
            _Lop = lop;
            _MaLop = malop;
            _BuoiSang = buoisang;
            _BuoiTrua = buoitrua;
            _BuoiChieu = buoichieu;
            _TongTien = tongtien;
        }
    }
}
