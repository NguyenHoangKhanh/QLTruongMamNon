﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NienKhoa
    {
        private string _NamHoc;
        private string _NamHoc1;
        private string _Khoa;
        private string _NgayKhaiGiang;
        private string _NgayKetThuc;

        public string NamH
        {
            get { return _NamHoc; }
            set { _NamHoc = value; }
        }
        public string NamH1
        {
            get { return _NamHoc1; }
            set { _NamHoc1 = value; }
        }
        public string Khoa
        {
            get { return _Khoa; }
            set { _Khoa = value; }
        }
        public string NgayKhaiG
        {
            get { return _NgayKhaiGiang; }
            set { _NgayKhaiGiang = value; }
        }
        public string NgayKetT
        {
            get { return _NgayKetThuc; }
            set { _NgayKetThuc = value; }
        }

      
        public NienKhoa(string namhoc, string khoa, string ngaykhaigiang,string ngayketthuc)
        {
            _NamHoc = namhoc;
            _Khoa = khoa;
            _NgayKhaiGiang = ngaykhaigiang;
            _NgayKetThuc = ngayketthuc;
        }

    }
}
