﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class ChiaPhong1
    {
        private string _NamHoc;
        private string _Lop;
        private string _MaNV;
        private string _TenNV;
        private string _MaLop;

        public string NamHoc
        {
            get
            {
                return _NamHoc;
            }

            set
            {
                _NamHoc = value;
            }
        }

        public string Lop
        {
            get
            {
                return _Lop;
            }

            set
            {
                _Lop = value;
            }
        }

        public string MaNV
        {
            get
            {
                return _MaNV;
            }

            set
            {
                _MaNV = value;
            }
        }

        public string TenNV
        {
            get
            {
                return _TenNV;
            }

            set
            {
                _TenNV = value;
            }
        }

        public string MaLop
        {
            get
            {
                return _MaLop;
            }

            set
            {
                _MaLop = value;
            }
        }

        public ChiaPhong1(string namhoc,string lop,string manv,string tennv,string malop)
        {
            _NamHoc = namhoc;
            _Lop = lop;
            _MaNV = manv;
            _TenNV = tennv;
            _MaLop = malop;
        }
    }
}
