﻿use QLMN
go
--Store Data
create proc loadData
as
begin 
	select * from NienKhoa
end

--Store insertData
create proc insertD
@Namhoc nvarchar(100),
@Khoa nvarchar(20),
@NgayKhaiGiang text,
@NgayKetThuc text
as
begin 
	insert into NienKhoa(Namhoc,Khoa,NgayKhaiGiang,NgayKetThuc) values (@Namhoc,@Khoa,@NgayKhaiGiang,@NgayKetThuc)
end

--Store deleteData
create proc deleteData
@Namhoc nvarchar(100)
as
begin 
	delete from NienKhoa where NienKhoa.Namhoc=@Namhoc
end

--check
create proc checkData
@Namhoc nvarchar(100)
as
begin 
	Select Namhoc from NienKhoa Where NienKhoa.Namhoc=@Namhoc 
end

--loadData1
create proc LoadData01
@Namhoc nvarchar(100)
as
begin 
	select MaLop,TenLop  from Khoi where NamHoc=@Namhoc
end

--Select
create proc SelectDaTa
as
begin
select Namhoc from NienKhoa 
end 

--update
create proc updateKhoi
@TenLop nvarchar (50),
@MaLop nvarchar(100) 
as
begin
	Update Khoi set TenLop=@TenLop  where MaLop=@MaLop
end

--update
create proc updateCDDD
@TenLop nvarchar (50),
@MaLop nvarchar(100)
as
begin
	Update CheDoDanhDu  set Lop=@TenLop  where MaLop=@MaLop
end

create proc updateDDNV
@TenLop nvarchar (50),
@MaLop nvarchar(100)
as
begin
	Update DiemDanhGV  set Lop=@TenLop  where MaLop=@MaLop
end
create proc updateChiaNV
@TenLop nvarchar (50),
@MaLop nvarchar(100)
as
begin
	Update ChiaNV set Lop=@TenLop  where MaLop=@MaLop
end
create proc updateDiemDanhHS
@TenLop nvarchar (50),
@MaLop nvarchar(100)
as
begin
	Update DiemDanhHS  set Lop=@TenLop  where MaLop=@MaLop
end
create proc updateChiaHS
@TenLop nvarchar (50),
@MaLop nvarchar(100)
as
begin
	Update ChiaHS set Lop=@TenLop  where MaLop=@MaLop
end

--Store insertKhoi
create proc insertKhoi
@Namhoc nvarchar(100),
@TenLop nvarchar(50),
@MaLop nvarchar(100)
as
begin 
	insert into Khoi(MaLop,TenLop,Namhoc) values (@MaLop,@TenLop,@Namhoc)
end

--check1
create proc checkData1
@MaLop nvarchar (100)
as
begin 
	Select MaLop from Khoi where MaLop=@MaLop
end

--Store deleteKhoi
create proc deleteKhoi
@MaLop nvarchar(100),
@NamHoc nvarchar(100)
as
begin 
	delete from Khoi  where  MaLop=@MaLop and NamHoc=@NamHoc
end

--store LoadHS
create proc LoadHS
as
begin
	Select * from HS
end
--check
create proc checkMaHS
@MaHS nvarchar(50)
as
begin 
	Select MaHS from HS where MaHS=@MaHS
end 

-store insertHS
create proc insertHS
@MaHS nvarchar(50),
@HoTen nvarchar(100),
@GioiTinh nvarchar(50),
@NgaySinh text,
@SDTLL nvarchar(50),
@HoTenBo nvarchar(100),
@NgheNghiepBo1 nvarchar(100),
@EmailBo nvarchar(100),
@HoTenMe nvarchar(100),
@NgheNghiepMe nvarchar(100),
@EmailMe nvarchar(100),
@DiaChi nvarchar(100)
as
begin
	insert into HS(MaHS,HoTen,GioiTinh,Ngaysinh,SDTLienLac,HoTenBo,NgheNghiepBo,EmailBo,HoTenMe,NgheNghiepMe,EmailMe,DiaChi) values (@MaHS,@HoTen,@GioiTinh,@NgaySinh,@SDTLL,@HoTenBo,@NgheNghiepBo1,@EmailBo,@HoTenMe,@NgheNghiepMe,@EmailMe,@DiaChi)
end

-store updateHS
create proc updateHS
@MaHS nvarchar(50),
@HoTen nvarchar(100),
@GioiTinh nvarchar(50),
@NgaySinh text,
@SDTLL nvarchar(50),
@HoTenBo nvarchar(100),
@NgheNghiepBo1 nvarchar(100),
@EmailBo nvarchar(100),
@HoTenMe nvarchar(100),
@NgheNghiepMe nvarchar(100),
@EmailMe nvarchar(100),
@DiaChi nvarchar(100)
as
begin
	Update HS  set HoTen=@HoTen , GioiTinh=@GioiTinh,Ngaysinh=@NgaySinh,SDTLienLac=@SDTLL,HoTenBo=@HoTenBo,NgheNghiepBo=@NgheNghiepBo1,EmailBo=@EmailBo,HoTenMe=@HoTenMe,NgheNghiepMe=@NgheNghiepMe,EmailMe=@EmailMe,DiaChi=@DiaChi where MaHS=@MaHS
end

-store updateĐiemanhHS
create proc updateDDHS1
@MaHS nvarchar(50),
@HoTen nvarchar(100)
as
begin
	Update DiemDanhHS  set TenHS=@HoTen where MaHS=@MaHS
end


-store updateĐiemanhHS
create proc updateChiaHS1
@MaHS nvarchar(50),
@HoTen nvarchar(100)
as
begin
	Update ChiaHS  set TenHS=@HoTen where MaHS=@MaHS
end

-store updateĐiemanhHS
create proc updateHPHS
@MaHS nvarchar(50),
@HoTen nvarchar(100)
as
begin
	Update HocphiHS  set TenHS=@HoTen where MaHS=@MaHS
end



--store delete
create proc deleteHS
@MaHS nvarchar(50)
as
begin
	delete from HS where  MaHS=@MaHS
end

--store LoadGV
create proc LoadGV
as
begin
	Select * from NhanVien
end
--check
create proc checkMaGV
@MaGV nvarchar(50)
as
begin 
	Select MaGV from NhanVien where MaGV=@MaGV
end 

--store insertGV
create proc insertGV
@MaGV nvarchar(50),
@TenGV nvarchar(100),
@GioiTinh nvarchar(50),
@NgaySinh text,
@BangCap nvarchar(100),
@DiaChi nvarchar(200),
@DienThoai nvarchar(100),
@TinhTrang nvarchar(100)
as
begin
	insert into NhanVien(MaGV,TenGV,NgaySinh,Gioitinh,BangCap,DiaChi,DienThoai,TinhTrang) values (@MaGV,@TenGV,@NgaySinh,@GioiTinh,@BangCap,@DiaChi,@DienThoai,@TinhTrang)
end

--store delete
create proc deleteGV
@MaGV nvarchar(50)
as
begin
	delete from NhanVien where  MaGV=@MaGV
end

--store updateGV
create proc updateGV
@MaGV nvarchar(50),
@TenGV nvarchar(100),
@GioiTinh nvarchar(50),
@NgaySinh text,
@BangCap nvarchar(100),
@DiaChi nvarchar(200),
@DienThoai nvarchar(100),
@TinhTrang nvarchar(100)
as
begin
	Update NhanVien  set TenGV=@TenGV , Gioitinh=@GioiTinh, NgaySinh=@NgaySinh,BangCap=@BangCap,DiaChi=@DiaChi,DienThoai=@DienThoai,TinhTrang=@TinhTrang  where MaGV=@MaGV
end

--store updateGV
create proc updateGV
@MaGV nvarchar(50),
@TenGV nvarchar(100),
@GioiTinh nvarchar(50),
@NgaySinh text,
@BangCap nvarchar(100),
@DiaChi nvarchar(200),
@DienThoai nvarchar(100),
@TinhTrang nvarchar(100)
as
begin
	Update NhanVien  set TenGV=@TenGV , Gioitinh=@GioiTinh, NgaySinh=@NgaySinh,BangCap=@BangCap,DiaChi=@DiaChi,DienThoai=@DienThoai,TinhTrang=@TinhTrang  where MaGV=@MaGV
end

---
create proc updateHD
@MaGV nvarchar(50),
@TenGV nvarchar(100)
as
begin
	Update HopDongNV  set TenNV=@TenGV where MaNV=@MaGV
end
---
create proc updateChiaNV1
@MaGV nvarchar(50),
@TenGV nvarchar(100)
as
begin
	Update ChiaNV  set TenNV=@TenGV where MaNV=@MaGV
end
---
create proc updateDDGV1
@MaGV nvarchar(50),
@TenGV nvarchar(100)
as
begin
	Update DiemDanhGV  set TenGV=@TenGV where MaGV=@MaGV
end
---
create proc updateLuongcoban
@MaNV nvarchar(50),
@TenNV nvarchar(100)
as
begin
	Update LuongCoBan  set TenNV=@TenNV where MaNV=@MaNV
end

--Select1
create proc SelectDaTa1
@Namhoc nvarchar(100)
as
begin
select TenLop from Khoi where  NamHoc=@Namhoc
end 

--Load MANv, TenNV
create proc LoadMaTenNV
as
begin
	select MaGV,TenGV,TinhTrang from NhanVien 
end
--Load MaNV,TenNV1
create proc LoadMaTenNV1
@NamHoc nvarchar(100),
@Lop nvarchar(50)
as
begin
	select MaNV,TenNV from ChiaNV where NamHoc=@NamHoc and Lop=@Lop
end

--insert ChiaNV
create proc insertChiaNV
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaNV nvarchar(50),
@TenNV nvarchar(100),
@MaLop nvarchar(100)
as
begin
	insert into ChiaNV(NamHoc,Lop,MaNV,TenNV,MaLop) values(@NamHoc,@Lop,@MaNV,@TenNV,@MaLop)
end

--delete ChiaNV
create proc deleteChiaNV
@MaNV nvarchar(50)
as
begin
	delete from ChiaNV where  MaNV=@MaNV 
end


--check
create proc checkMaNV
@MaNV nvarchar(50),
@NamHoc nvarchar(100)
as
begin 
	Select * from ChiaNV where MaNV=@MaNV and NamHoc=@NamHoc 
end 
--TimKiem
create proc TimKiem
@MaNV nvarchar(50),
@TenNV nvarchar(100)
as
begin
	select MaGV,TenGV,TinhTrang from NhanVien where MaGV like ''+@MaNV+'%' or TenGV like''+@TenNV+'%'
end

--Load MANv, TenNV
create proc LoadMaTenHS
as
begin
	select MaHS,HoTen from HS 
end
--Load MaNV,TenNV1
create proc LoadMaTenHS1
@NamHoc nvarchar(100),
@Lop nvarchar(50)
as
begin
	select MaHS,TenHS from ChiaHS where NamHoc=@NamHoc and Lop=@Lop
end

--insert ChiaHS
create proc insertChiaHS
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaHS nvarchar(50),
@TenHS nvarchar(100),
@MaLop nvarchar(100)
as
begin
	insert into ChiaHS(NamHoc,Lop,MaHS,TenHS,MaLop) values(@NamHoc,@Lop,@MaHS,@TenHS,@MaLop)
end

--delete ChiaNV
create proc deleteChiaHS
@MaHS nvarchar(50)
as
begin
	delete from ChiaHS where  MaHS=@MaHS
end

--check
create proc checkMaHS1
@MaHS nvarchar(50),
@NamHoc nvarchar(100)
as
begin 
	Select * from ChiaHS where MaHS=@MaHS and NamHoc=@NamHoc 
end

--TimKiem
create proc TimKiem1
@MaHS nvarchar(50),
@TenHS nvarchar(100)
as
begin
	select MaHS,HoTen from HS where MaHS like ''+@MaHS+'%' or HoTen like''+@TenHS+'%'
end

--TenNV
create proc TenNV
@NamHoc nvarchar(100),
@Lop nvarchar(50)
as
begin
	select TenNV from ChiaNV where NamHoc=@NamHoc and Lop=@Lop
end
--LoadDD
create proc LoadDD
@Tuan nvarchar(50),
@Thu text,
@NamHoc nvarchar(100),
@Lop nvarchar (50)
as
begin
	select MaGV,TenGV from DiemDanhGV where Tuan=@Tuan and Thu like @Thu and NamHoc=@NamHoc and Lop=@Lop
end
--insertDD
create proc insertDD
@Tuan nvarchar(50),
@Thu text,
@MaGV nvarchar(50),
@TenGV nvarchar(100),
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaLop nvarchar(100)
as
begin
	insert into DiemDanhGV(Tuan,Thu,MaGV,TenGV,NamHoc,Lop,MaLop) values (@Tuan,@Thu,@MaGV,@TenGV,@NamHoc,@Lop,@MaLop)
end
--deleteDD
create proc deleteDD
@MaGV nvarchar(50),
@Thu text
as
begin
	delete from DiemDanhGV where MaGV=@MaGV and Thu like @Thu
end
--check
create proc checkDD
@MaGV nvarchar(50),
@Thu text
as
begin 
	Select * from DiemDanhGV where MaGV=@MaGV and Thu like @Thu
end 


--LoadHS
create proc LoadHS1
@Tuan nvarchar(50),
@Thu text,
@NamHoc nvarchar(100),
@Lop nvarchar (50)
as
begin
	select MaHS,TenHS from DiemDanhHS where Tuan=@Tuan and Thu like @Thu and NamHoc=@NamHoc and Lop=@Lop
end
--insertDD
create proc insertDDHS
@Tuan nvarchar(50),
@Thu text,
@MaHS nvarchar(50),
@TenHS nvarchar(100),
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaLop nvarchar(100)
as
begin
	insert into DiemDanhHS(Tuan,Thu,MaHS,TenHS,NamHoc,Lop,MaLop) values (@Tuan,@Thu,@MaHS,@TenHS,@NamHoc,@Lop,@MaLop)
end
--deleteDD
create proc deleteDDHS
@MaHS nvarchar(50),
@Thu text
as
begin
	delete from DiemDanhHS where MaHS=@MaHS and Thu like @Thu
end
--check
create proc checkDDHS
@MaHS nvarchar(50),
@Thu text
as
begin 
	Select * from DiemDanhHS where MaHS=@MaHS and Thu like @Thu
end 

--LoadLuong
create proc LoadLuong
@MaNV nvarchar(50)
as
begin
	select NgayBatDau,LuongCoBan,LuongBHXH,PhuCap1,PhuCap2,PhuCap3 from LuongCoBan where MaNV=@MaNV
end

--insertLuong
create proc insertLuong
@MaNV nvarchar(50),
@TenNV nvarchar(100),
@NgayBatDau text,
@Luongcoban nvarchar(100),
@Luongbhxh nvarchar(100),
@phucap1 nvarchar(100),
@phucap2 nvarchar(100),
@phucap3 nvarchar(100)
as
begin 
	insert into LuongCoBan(MaNV,TenNV,NgayBatDau,LuongCoBan,LuongBHXH,PhuCap1,PhuCap2,PhuCap3) values (@MaNV,@TenNV,@NgayBatDau,@Luongcoban,@Luongbhxh,@phucap1,@phucap2,@phucap3)
end

--deleteluong
create proc DeleteLuong
@MaNV nvarchar(50),
@NgayBatDau text
as
begin
	delete from LuongCoBan where MaNV=@MaNV and NgayBatDau like @NgayBatDau
end

--LoadHD
create proc LoadHopDong
@MaNV nvarchar(50)
as
begin
	select NgayBatDau,NgayKetThuc from HopDongNV where MaNV=@MaNV
end

--insertHD
create proc insertHD
@MaNV nvarchar(50),
@TenNV nvarchar(100),
@NgayBatDau text,
@NgayKetThuc text
as
begin 
	insert into HopDongNV(MaNV,TenNV,NgayBatDau,NgayKetThuc) values (@MaNV,@TenNV,@NgayBatDau,@NgayKetThuc)
end

--deleteluong
create proc DeleteHD
@MaNV nvarchar(50)
as
begin
	delete from HopDongNV where MaNV=@MaNV 
end

--LoadGVVang
create proc LoadGVVang
@MaGV nvarchar(50)
as
begin
	select Tuan,Thu,Lop from DiemDanhGV where MaGV=@MaGV
end
--update
create proc updatePhongNV
@MaNV nvarchar(50),
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaLop nvarchar(100)
as
begin
	Update ChiaNV set NamHoc=@NamHoc , Lop=@Lop , MaLop=@MaLop where MaNV=@MaNV
end


--LoadHP
create proc LoadHP
@MaHS nvarchar(50)
as
begin
	select Ngaydonghocphi,Hocphi,Ngaybatdauhoc,Ngayketthuc from HocphiHS where MaHS=@MaHS
end

--insertHP
create proc insertHP
@MaHS nvarchar(50),
@TenHS nvarchar(100),
@Ngaydonghocphi text,
@Hocphi nvarchar(100),
@Ngaybatdauhoc text,
@Ngayketthuc text
as
begin 
	insert into HocphiHS(MaHS,TenHS,Ngaydonghocphi,Hocphi,Ngaybatdauhoc,Ngayketthuc) values (@MaHS,@TenHS,@Ngaydonghocphi,@Hocphi,@Ngaybatdauhoc,@Ngayketthuc)
end

--deleteHP
create proc DeleteHP
@MaHS nvarchar(50)
as
begin
	delete from HocphiHS where MaHS=@MaHS 
end

--LoadHSVang
create proc LoadHSVang
@MaHS nvarchar(50)
as
begin
	select Tuan,Thu,Lop from DiemDanhHS where MaHS=@MaHS
end
--update
create proc updatePhongHS
@MaHS nvarchar(50),
@NamHoc nvarchar(100),
@Lop nvarchar(50),
@MaLop nvarchar(100)
as
begin
	Update ChiaHS set NamHoc=@NamHoc , Lop=@Lop , MaLop=@MaLop where MaHS=@MaHS
end

--LoadCDDD
create proc LoadCDDD
as
begin
	select * from CheDoDanhDu
end

--insertCDDD
create proc insertCDDD
@NamHoc nvarchar(100),
@Tuan nvarchar(50),
@Ngay text,
@Lop nvarchar(50),
@MaLop nvarchar(100),
@BuoiSang nvarchar(200),
@BuoiTrua nvarchar(200),
@BuoiChieu nvarchar(200),
@TongTien nvarchar(100)
as
begin 
	insert into CheDoDanhDu(NamHoc,Tuan,Ngay,Lop,MaLop,Buoisang,Buoitrua,Buoichieu,Tongtien) values (@NamHoc,@Tuan,@Ngay,@Lop,@MaLop,@BuoiSang,@BuoiTrua,@BuoiChieu,@TongTien)
end

--deleteCDDD
create proc deleteCDDD
@MaLop nvarchar(100)
as
begin
	delete from CheDoDanhDu where MaLop=@MaLop
end

--checkCDDD
create proc checkCDDD
@MaLop nvarchar(100)
as
begin 
	Select * from CheDoDanhDu where MaLop=@MaLop
end

--TimKiem2
create proc TimKiem2
@MaNV nvarchar(50),
@TenNV nvarchar(100)
as
begin
	select * from NhanVien where MaGV like ''+@MaNV+'%' or TenGV like''+@TenNV+'%'
end

--TimKiem3
create proc TimKiem3
@MaHS nvarchar(50),
@TenHS nvarchar(100)
as
begin
	select * from HS where MaHS like ''+@MaHS+'%' or HoTen like''+@TenHS+'%'
end

--
create proc LoadLop
@MaHS nvarchar(50)
as
begin 
	select Lop from ChiaHS where MaHS=@MaHS
end
--
create proc LoadLopGV
@MaNV nvarchar(50)
as
begin 
	select Lop from ChiaNV where MaNV=@MaNV
end

--
create proc insertDN
@ID nvarchar(100),
@MK nvarchar(100),
@Cap nchar(10)
as 
begin
	insert into DN(ID,MK,Cap) values (@ID,@MK,@Cap)
end

-- 
create proc checkDN
@ID nvarchar(100)
as
begin
	select * from DN where ID = @ID
end
--
create proc checkDN1
@ID nvarchar(100),
@MK nvarchar(100)
as
begin
	select * from DN where ID = @ID and MK = @MK
end
--
create proc updateDN
@ID nvarchar(100),
@MK nvarchar (100)
as
begin
	Update DN set MK=@MK  where ID=@ID
end
--

create proc deleteDN
@ID nvarchar(100)
as
begin
	delete from DN where ID=@ID and Cap like '1'
end
